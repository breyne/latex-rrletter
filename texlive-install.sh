#!/bin/sh

######################################################################
RED='\033[0;33m'
NC='\033[0m' # No Color

######################################################################

dir=$(pwd)

echo -e ${RED}
echo [rrletter] LINK FILES TO LOCAL TEXMF
echo [rrletter] Initialize usertree
echo -e ${NC}
tlmgr init-usertree

#cd ~/texmf
cd $(kpsewhich -var-value TEXMFHOME) 
mkdir -p tex/latex/local/ && cd tex/latex/local/

######################################################################
echo -e ${RED}
echo [rrletter] source directory: $dir
echo [rrletter] destination dir : $(pwd)
echo -e ${NC}

ln -s ${dir}/rrletter.cls .

echo -e ${RED}
echo [rrletter] rebuild index
echo -e ${NC}
texhash $(kpsewhich -var-value TEXMFHOME)

echo -e ${RED}
echo [rrletter] Over. 
echo -e ${NC}
