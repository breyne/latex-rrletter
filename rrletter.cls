%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%%              ------------------------------
%%               Revise and Resubmit - Letter
%%              ------------------------------
%%
%% DESCRIPTION =======================================================
%% 
%% A set of minimal tools for typesetting a letter of revision and resubmission
%% with little dependencies.
%%
%%
%% PROVIDED COMMANDS =================================================
%% ----- Typesetting main body ---------------------------------------
%%
%% \articleref{<string>} Define the article reference string of characters
%%
%% \printarticleref{}    Print the article reference string of characters
%%
%% \newreviewer{}        Section-like command
%%                       with possibility of using \label{<ref>}
%%
%% \printreviewer{<ref>} Print the referenced reviewer 
%%                       or the current reviewer if no argument is provided
%%
%% ----- Typesetting comments ----------------------------------------
%% 
%% \newcomment{<com>}{<ans>} Typeset the comment in a quote environment,
%%                           then the answer.
%%                           Possibility of using \label{<ref>}.
%%
%% \printcomment{<ref>}      Print the referenced comment
%%                           or the current reviewer if no argument is provided.
%%
%% \noanswer{}               Before calling \newcomment{}{}, 
%%                           the answer is not printed, neither is the warning
%%
%% \nocommentnumber{}        Before calling \newcomment{}{}, 
%%                           the comment does not have a number
%%
%% \begin{commentgroup}      Introduces sub numbering for all \newcomment within.
%% ... \end{commentgroup}
%%
%% ----- Customizing -------------------------------------------------
%%
%% \commentstyle{<style>} style for each comment environment.
%%
%% \answerstyle{<style>}  style for each answer environment.
%%
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% ==================================================================================================
% BASE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ==================================================================================================

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{rrletter}[Revise and resubmit letter]
% -----
\LoadClass[12pt]{letter}
\RequirePackage{xcolor}    % 

% ==================================================================================================
%% Response tools %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ==================================================================================================

\def\theArticleRef{aaa}
\newcommand\articleref[1]{\def\theArticleRef{#1}}
\newcommand{\printarticleref}{\tt\theArticleRef{}}

% ==================================================================================================
% Below we define what is needed to start a new reviewer section

\def\reviewername{reviewer}
\def\reviewerfont{\scshape}

\newcounter{rev}
\renewcommand{\therev}{\textsc{\roman{rev}}}% clarifies references

\newcommand{\printreviewer}[1]{%
 {\reviewerfont\reviewername{}\ }%
 \def\tmp{#1}%
 \ifx\tmp\empty%
  \therev%
 \else%
  \ref{#1}%
 \fi%
}

\newcommand\newreviewer{
 \refstepcounter{rev}
 %\clearpage
 \hrulefill~{\large\printreviewer{}}~\hrulefill
 \par
}

% ==================================================================================================
% Below we define the custom styling commands, set empty by default

\newcommand\RRcommentstyle{\itshape}
\newcommand\commentstyle[1]{
 \renewcommand\RRcommentstyle{#1}
}

\newcommand\RRanswerstyle{}
\newcommand\anserstyle[1]{
 \renewcommand\RRanswerstyle{#1}
}

% ==================================================================================================
% Below we define what is needed to typeset comments and answers


\newcounter{com}[rev]% set to zero whenever rev is increased
\renewcommand{\thecom}{\therev-\arabic{com}}% clarifies references
\newcounter{subcom}[com]% set to zero whenever com is increased
\renewcommand{\thesubcom}{\thecom.\arabic{subcom}}

\newif\ifsamecomment
\samecommentfalse

% Environment commentgroup will introduce sub-numbering for comments
\newenvironment{commentgroup}{
 \stepcounter{com}
 \samecommenttrue
}{
 \samecommentfalse
}

% \noanswer will turn off warning for empty answers
\newif\ifnoanswerwarning
\noanswerwarningfalse
\def\noanswer{\noanswerwarningtrue}

% \nocommentnumber will turn off warning for empty answers
\newif\ifcomnumber
\comnumbertrue
\def\nocommentnumber{\comnumberfalse}

% \newcomment will typeset comments and ansers
% Below we use egregs answer on stackexchange to test the argument's printed size
%https://tex.stackexchange.com/a/53091/153854

\newcommand\newcomment[2]{
 % Figuring out the com number ---------------------------------------
 \ifcomnumber
  \ifsamecomment%
   \refstepcounter{subcom}%
   \def\printCommentNumber{\thesubcom}%
  \else%
   \refstepcounter{com}%
   \def\printCommentNumber{\thecom}%
  \fi%
 \fi%
 % Writing the comment contents --------------------------------------
 \begin{quote}
 \ifcomnumber%
  \printCommentNumber{}.~%
 \fi%
  \setbox0=\hbox{\detokenize{#1}\unskip}\ifdim\wd0=0pt
   {\color{red}\textsc{warning---arg 1}: missing comment.}
  \else %case: non empty
   \RRcommentstyle
   #1%
  \fi
 \end{quote}
 % Writing the answer ------------------------------------------------
 \ifnoanswerwarning% nothing if noanswer was used
  \noanswerwarningfalse
 \else
  \setbox0=\hbox{\detokenize{#2}\unskip}\ifdim\wd0=0pt
  {\color{red}\textsc{warning---arg 2}: missing answer to comment%
  \ifcomnumber%
   ~\printCommentNumber{}.
  \else%
   .
  \fi
  Use \textbackslash\texttt{noanswer}\textbackslash\texttt{newcomment\{...\}\{\}} to switch that off.}
  \else %case: non empty
   \RRanswerstyle
   #2
  \fi
 \fi
 \comnumbertrue
}

\newcommand\printcomment[1]{
 \def\tmp{#1}%
 \ifx\tmp\empty
  comment \printCommentNumber{}%
 \else
  comment \ref{#1}%
 \fi
}


% ==================================================================================================
% BIBLIOGRAPHY: FOOTCITE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ==================================================================================================

% ---------- Basic declarations 

\RequirePackage{csquotes}
\RequirePackage[backend=biber,style=numeric-comp]{biblatex}
\RequirePackage{manyfoot}

% ---------- Redefine \footcite

% This implementation was found at 
% https://tex.stackexchange.com/questions/20787/biblatex-cite-with-footnote-only-once-with-use-of-brackets

\ExecuteBibliographyOptions{citetracker=true,sorting=none}

% Citation footnotes: use \footnoteA
\DeclareNewFootnote{A}

% 
% Number of each bibliography entry in brackets
\DeclareFieldFormat{labelnumberwidth}{\mkbibbrackets{#1}}

\makeatletter

\newtoggle{cbx@togcite}
    
% Citation number superscript in brackets
\renewcommand\@makefntext[1]{%
 \iftoggle{cbx@togcite}
  {\@textsuperscript{\normalfont[\@thefnmark]}\enspace #1}
  {\@textsuperscript{\normalfont\@thefnmark}\enspace #1}%
 \global\togglefalse{cbx@togcite}
}
% Citation number superscript in brackets (for babel french)
\ifdef{\@makefntextFB}{%
 \renewcommand\@makefntextFB[1]{%
  \iftoggle{cbx@togcite}
   {\@textsuperscript{\normalfont[\@thefnmark]}\enspace #1}
   {\@textsuperscript{\normalfont\@thefnmark}\enspace #1}%
  \global\togglefalse{cbx@togcite}
 }
}{}
    
%---------------------------------------------------------------
% Mostly verbatim from Joseph Wright
% http://www.texdev.net/2010/03/08/biblatex-numbered-citations-as-footnotes/
    
\DeclareCiteCommand{\footcite}[\cbx@superscript]{%
 \usebibmacro{cite:init}%
 \let\multicitedelim=\supercitedelim%
 \iffieldundef{prenote}{}{\BibliographyWarning{Ignoring prenote argument}}%
 \iffieldundef{postnote}{}{\BibliographyWarning{Ignoring postnote argument}}%
}{%
 \usebibmacro{citeindex}%
 \usebibmacro{footcite}%
 \usebibmacro{cite:comp}%
}{
 %
}{
 \usebibmacro{cite:dump}%
}

\newbibmacro*{footcite}{%
 \ifciteseen{%
  %
 }{%
  \xappto\cbx@citehook{%
   \global\toggletrue{cbx@togcite}%
   \noexpand\footnotetextA[\thefield{labelnumber}]{\fullcite{\thefield{entrykey}}\addperiod}%
  }
 }
}

\newrobustcmd{\cbx@superscript}[1]{%
 \mkbibsuperscript{\mkbibbrackets{#1}}%
 \cbx@citehook%
 \global\let\cbx@citehook=\empty%
}%

\let\cbx@citehook=\empty
    
\makeatother

