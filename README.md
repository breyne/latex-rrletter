# latex-rrletter

*Minimal tools for letters of revision, resubmission and/or review.*

---

This class is built on top of `letter`, to which it adds a set of simple macros. They are intended for easily typesetting exchanges during a peer-reviewing process:

- numbered and referenced reviewers (acting as sections),
- numbered (or not) and referenced comments
- with or without answers.

### Comments

Style and layout customization are currently not supported and necessitate the  redefinition of commands.
Adding hooks and options could be a perspective.

The requirements are rather low, except for the bibliography management that requires `biblatex` and a compilation with `biber`.  Since it is only used to define a footnote citation command, removing it will not impact the rest of the class.

Comments and sources are specified within the implementation of `rrletter.cls`. An example of use with the details of possibilities is proposed in `example-rrletter.{tex,pdf}`.

